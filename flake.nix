{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/22.05";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
   flake-utils.lib.eachDefaultSystem (system:
    let
      #system = "x86_64-linux";
      pkgs = import nixpkgs { inherit system; };
      version = "v0.0";

      flakeImage = pkgs.dockerTools.pullImage {
        imageName = "nixpkgs/nix-flakes";
        imageDigest =
          "sha256:653ac11d23bbe5b9693f156cafeb97c7e8000b187d1bafec3798f6c92238fde2";
        sha256 = "15543hvgw2g8aadkx335pprrxq3ldcv93a9qq9c4am0rbkw8prrw";
        finalImageName = "nixpkgs/nix-flakes";
        finalImageTag = "nixos-21.11";
      };
    in
    rec {
      packages = rec {
        mdbook-admonish = pkgs.callPackage ./pages/mdbook-admonish.nix { };

        doc = pkgs.stdenv.mkDerivation {
          name = "pages_tuto";
          src = ./pages;
          nativeBuildInputs = with pkgs; [ mdbook mdbook-mermaid mdbook-admonish ]; # mdbook-linkcheck pour checker la validiter des liens
          buildCommand = ''
            mkdir $out
            cp -r --no-preserve=mode $src/* .
            mdbook build -d $out
          '';
        };

        tuto-doc-docker = pkgs.dockerTools.buildImageWithNixDb {
          name = "registry.gitlab.inria.fr/control-for-computing/tutorial/doc";
          tag = version;
          fromImage = flakeImage;
          contents = with pkgs; [ mdbook mdbook-mermaid mdbook-admonish ]; # mdbook-linkcheck pour checker la validiter des liens
        };
        
        slides = pkgs.stdenv.mkDerivation {
          name = "slides-tuto";
          src = ./slides;
          buildInputs = with pkgs; [
            texlive.combined.scheme-full
            pandoc
            rubber
          ];
          buildPhase = ''
            pandoc --from=markdown --to=beamer --slide-level=1 --template=template.tex --output=main.tex main.md
            rubber -d main.tex
          '';
          installPhase = ''
            mkdir -p $out
            cp main.pdf $out/slides.pdf
          '';
        };

        slides-docker = pkgs.dockerTools.buildImageWithNixDb {
          name = "registry.gitlab.inria.fr/control-for-computing/tutorial/slides";
          tag = version;
          fromImage = flakeImage;
          contents = with pkgs; [ 
            texlive.combined.scheme-full
            pandoc
            rubber
          ];
        };


      };
      devShells = {
        pages = pkgs.mkShell {
          buildInputs = [
            pkgs.mdbook
            pkgs.mdbook-mermaid
            packages.${system}.mdbook-admonish
          ];
        };
      };
    });
}

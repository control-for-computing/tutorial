
VERSION=$1

nix build .#slides-docker
docker load < result
docker push registry.gitlab.inria.fr/control-for-computing/tutorial/slides:$VERSION

nix build .#tuto-doc-docker
docker load < result
docker push registry.gitlab.inria.fr/control-for-computing/tutorial/doc:$VERSION

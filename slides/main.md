---
title: 'Under Control: A Control Theory Introduction for Computer Scientists'
short_title: 'Under Control: A Control Theory Introduction for Computer Scientists'
subtitle: 'Session \@ ACSOS 2024'
date: 16/09/2024
authors:
  - firstname: "Quentin"
    lastname: "Guilloteau"
    mark: "$^{4}$"
  - firstname: "Sophie"
    lastname: "Cerf"
    mark: "$^{2}$"
    is_presenter: true
  - firstname: "Eric"
    lastname: "Rutten"
    mark: "$^{1}$"
  - firstname: "Raphaël"
    lastname: "Bleuse"
    mark: "$^{1}$"
  - firstname: "Bogdan"
    lastname: "Robu"
    mark: "$^{3}$"
affiliations:
  - mark: "$^{1}$"
    signature: "Univ. Grenoble Alpes, Inria, CNRS, LIG"
    short: "UGA"
    email: "Firstname.Lastname@inria.fr"
  - mark: "$^{2}$"
    signature: "Univ. Lille, Inria, Central Lille, CRIStAL"
    short: "INRIA"
    email: "Sophie.Cerf@inria.fr"
  - mark: "$^{3}$"
    signature: "Univ. Grenoble Alpes, CNRS, G-INP, Gipsa Lab"
    short: "G-INP"
    email: "Bogdan.Robu@grenoble-inp.fr"
  - mark: "$^{4}$"
    signature: "University of Basel, Switzerland"
    short: "UniBas"
    email: "Quentin.Guilloteau@unibas.ch"

header-includes:
    - \usepackage{tikz}
    - \usepackage{hyperref}
    - \usepackage{subcaption}
    - \usepackage{MnSymbol,wasysym}
# lang: en
---

# Goal of this tutorial

<!---
- Present the need for runtime management of computing systems

- Show how computing systems can benefit from the Feedback loop approach
--->

- Show what Control Theory can bring to Autonomic Computing
  - principles of the guarantees

- Make Control Theory concepts accessible to computer scientists 
  - (almost) mathematical-free

- Present Control's reusable methodology

- Hands-on, interactive, and reusable implementation (**NOT** a theoretical approach, see Papadopoulos@ACSOS'22)

- Formulate your own Control problem !

- Give keys to discuss and collaborate with Control Scientists


# The need for Regulation

- Complex systems

  - Crazy software stacks on crazy hardware
    
  - Many sources of unpredictability, variabilities

  - $\rightsquigarrow$ need for runtime management
  
- Common approaches:

  - model and configure offline
  
    - optimal problem solving
    
    - very complex
    
    - difficult to apprehend runtime events 
   
  - adhoc runtime policies
  
    - *if this then that*
    
    - very arbitrary constants
    
    - no guarantee

\begin{center}
\textbf{We need another approach:} with guarantees, simple, and robust to runtime variations
\end{center}
  

# The Feedback Loop

## The idea

Use the measurement of relevant system metrics to adapt its configuration

## Autonomic Computing (IBM 2003)

:::::::::::::: {.columns}
::: {.column width="50%"}
![MAPE-K Loop](figs/mapek.jpg){width=70% height=35%}
:::
::: {.column width="50%"}


- auto-*

  - configuration
  
  - healing
  
  - **optimization**
  
  - protection
  
- main formulation: MAPE-K

- Separation of concerns



:::
::::::::::::::


# The Feedback Loop

## The idea

Use the measurement of relevant system metrics to adapt its configuration

## Autonomic Computing

:::::::::::::: {.columns}
::: {.column width="50%"}
![Autonomic Computing Techniques (Porter et al.@ACSOS'20)](figs/Porter-survey.png){width=90% height=35%}
:::
::: {.column width="50%"}


- auto-*

  - configuration
  
  - healing
  
  - **optimization**
  
  - protection
  
- main formulation: MAPE-K

- Separation of concerns

- $\neq$ implementations of AC (rules, AI, **control**)


:::
::::::::::::::






# Control Theory

::: {.block}
## Goal

Soundly act on a system based on its measurements, to reach a desired behavior

:::


:::::::::::::: {.columns}
::: {.column width="50%"}
![Control Loop](figs/control_loop.pdf){width=90% height=30%}
:::
::: {.column width="50%"}

- Handles dynamic behavior

- Proven Guarantees

  - Stability

  - Robustness

  - Precision

- Reusable design methodology  

- Long history in physical systems

- Promising results for CS
  


:::
::::::::::::::



# Control Theory Formulation in 1 slide

\begin{block}{First, \textbf{a Dynamic Model ...} (i.e., how does the system behave)}

\begin{center}
\scalebox{0.85}{
    $\displaystyle {\color{red}\textbf{y}}(k + 1) = \sum_{i = 0}^k a_i {\color{red}\textbf{y}}(k - i) + \sum_{j = 0}^k b_j {\color{blue}\textbf{u}}(k - j)$
}
\end{center}
\end{block}

\begin{block}{... then \textbf{a PID Controller} (i.e., the Closed-Loop behavior)}
\begin{center}
\scalebox{0.8}{
$\displaystyle {\color{blue}\textbf{u}}_k = \textbf{K}_p \times Error_k + \textbf{K}_i \times \sum_k Error_k + \textbf{K}_d \times \left(Error_k - Error_{k-1}\right)$
}
\end{center}
\end{block}

\begin{columns}
\begin{column}{0.47\textwidth}
\begin{block}{Sensors \& Actuators}
    \begin{itemize}
        \item Actuator: ${\color{blue}\textbf{u}}$
        \item Sensor: ${\color{red}\textbf{y}}$
        \item Error: $Reference - Sensor$
    \end{itemize}
\end{block}
\end{column}
\begin{column}{0.53\textwidth}

\begin{block}{Method}
    \begin{enumerate}
\item Open-Loop expe (predefined ${\color{blue}\textbf{u}}$)
\item Model parameters ($a_i, b_j$)
\item Choice controller behavior ($\textbf{K}_{*}$)
    \end{enumerate}
\end{block}
\end{column}
\end{columns}


# Settling Time, Overshoot and Error


![Closed Loop Behavior](figs/closed_loop_behavior.pdf){width=80% height=65%}


\begin{center}
The controller gains define this behavior!
\end{center}

# Controllers

## Goal

\begin{center}
Map the error to the next action, to reach desired system state with guaranted behavior
\end{center}

## Many (many) types of Controllers

- Feedback (PID, MFC, RST, etc.)

- Feedforward (proactive reaction to disturbances)

- Adaptive (change behavior at runtime)

- Model Predictive

- Event Based

- Optimal

- and more!

# Methodology


![Control Theory Methodology (Cerf, 2021)](figs/methodo.pdf){width=90% height=50%}

<!---
# Some Examples

- Regulate the **heat of a processor** based on its frequency

  - Sensor: CPU thermometer
  
  - Actuator: DVFS
  
  - Reference: Desired CPU temperature
  
- Regulate the **waiting time of users** based on the number of available servers

  - Sensor: Waiting time of a request
  
  - Actuator: Number of servers to add/remove
  
  - Reference: Mean waiting time for the requests
  
- Regulate the **FPS of an online video rendering** based on the quality

  - Sensor: FPS
  
  - Actuator: depth of computation
  
  - Reference: 60 FPS
  
--->
  
# Some Examples

- Regulate the **energy consumption of a processor** in memory-intensive phases

  - Sensor: application's progess
  
  - Actuator: RAPL powercap
  
  - Controller: PI, Adaptive control
  
- Regulate **grid unused resources** with harvesting and avoid bottlenecks

  - Sensor: Cluster availability, file-system load
  
  - Actuator: Number of low priority tasks injected
  
  - Controller: PI, Model-Free Control
  
- Regulate the **privacy threats** of mobile phone users

  - Sensor: privacy metric (based on users Points of Interests)
  
  - Actuator: transmited positions
  
  - Controller: PI, Optimal control

# Your turn! 

## What you will do now

1. Play with a dynamic system

2. Implement a naive Threshold-based controller 

3. First introduction with Control Theory: P Controller

4. Implement more precise controllers (PI, PID)

5. Perform a model identification

5. Apprehend more advanced control techniques

5. **Formalize a control closed-loop of your own system**

6. (Optional) Implement a PI controller on a real system


## Starting Point


\begin{center}
\url{https://tinyurl.com/Control4ComputingACSOS}
\end{center}


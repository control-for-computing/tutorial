# Publications


Here is a list of publications on the topic of Control for Computing:


## Methodology

- Hellerstein et al. **Feedback control of computing systems**, John Wiley & Sons, [pdf](https://www.academia.edu/download/64177806/J.L.Hellerstein,%20et%20al%20-%20Feedback%20Control%20Of%20Computing%20System.pdf)

- Filieri et al. **Software engineering meets control theory**, *2015 IEEE/ACM 10th International Symposium on Software Engineering for Adaptive and Self-Managing Systems*, [pdf](https://iliasger.github.io/pubs/2015-SEAMS-SEmeetsCT.pdf)

- Filieri et al. **Control Strategies for Self-Adaptive Software Systems**, *ACM Trans. Auton. Adapt. Syst. 11, 4 (2017)*, [pdf](https://www.antonio.filieri.name/publications/preprints/2017-taas.pdf)

- Litoiu et al. **What can control theory teach us about assurances in self-adaptive software systems?**, Springer Software Engineering for Self-Adaptive Systems III, [pdf](https://inria.hal.science/hal-01281063/file/fb-assurances.pdf)

- Rutten et al. **Feedback control as MAPE-K loop in autonomic computing**, *Springer Software engineering for self-adaptive systems 2017*, [pdf](https://hal.inria.fr/hal-01285014/document)

## Applications

- Cerf et al. **Sustaining Performance While Reducing Energy Consumption: A Control Theory Approach**, *EUROPAR 2021*, [pdf](https://hal.inria.fr/hal-03259316/file/main.pdf)

- Cerf et al. **Cost function based event triggered Model Predictive Controllers application to Big Data Cloud services**, *CDC 2016*, [pdf](https://hal.archives-ouvertes.fr/hal-01348687/file/FinalCDC16.pdf)

- Guilloteau et al. **Controlling the Injection of Best-Effort Tasks to Harvest Idle Computing Grid Resources**, *ICSTCC 2021*, [pdf](https://hal.inria.fr/hal-03363709/file/ICSTCC_2021.pdf)

# Contacts

The authors of this tutorial are:

- [Quentin GUILLOTEAU](https://guilloteauq.github.io/) (Quentin.Guilloteau AT univ-grenoble-alpes.fr)

- [Sophie CERF](https://sites.google.com/view/sophiecerf/) (Sophie.Cerf AT inria.fr)

- [Eric RUTTEN](http://team.inria.fr/ctrl-a/members/eric-rutten) (Eric.Rutten AT inria.fr)

- [Bogdan ROBU](http://www.gipsa-lab.grenoble-inp.fr/page_pro.php?vid=1451) (Bogdan.Robu AT grenoble-inp.fr)

- [Raphaël BLEUSE](https://research.bleuse.net/) (Raphael.Bleuse AT univ-grenoble-alpes.fr)

All the source code is available [here](https://gitlab.inria.fr/control-for-computing/tutorial).

Feel free to make contributions! :)

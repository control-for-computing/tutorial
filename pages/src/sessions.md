# Sessions

This tutorial has been given at the following events:

- 16/09/2024 - [ACSOS 2024](https://2024.acsos.org/details/acsos-2024-tutorials/1/Under-Control-A-Control-Theory-Introduction-for-Computer-Scientists) - Sophie Cerf ([`160924-acsos`](https://gitlab.inria.fr/control-for-computing/tutorial/-/tree/160924-acsos))

- 03/06/2024 - [FlexScience@HPDC24](https://sites.google.com/view/flexscience) - Quentin Guilloteau ([`030624-flexscience`](https://gitlab.inria.fr/control-for-computing/tutorial/-/tree/030624-flexscience))

- 14/03/2024 - [ANR ADAPT 2024](https://projects.femto-st.fr/ANR-ADAPT/welcome-adapt) - Sophie Cerf ([`140324-adapt`](https://gitlab.inria.fr/control-for-computing/tutorial/-/tree/140324-adapt))

- 13/12/2023 - [VELVET 2023](https://helene-coullon.fr/pages/velvet/) - Sophie Cerf ([`131223-velvet`](https://gitlab.inria.fr/control-for-computing/tutorial/-/tree/131223-velvet))

- 04/07/2023 - [ComPAS 2023](https://2023.compas-conference.fr/tutoriaux/) - Quentin Guilloteau ([`040723-compas`](https://gitlab.inria.fr/control-for-computing/tutorial/-/tree/040723-compas))

- 27/06/2023 - Spirals Seminar - Sophie Cerf ([`270623-spirals`](https://gitlab.inria.fr/control-for-computing/tutorial/-/tree/270623-spirals))

- 13/06/2023 - LIG - Quentin Guilloteau ([`130623-lig`](https://gitlab.inria.fr/control-for-computing/tutorial/-/tree/130623-lig))

- 21/04/2023 - Ctrl-A Seminar - Quentin Guilloteau ([`210423-ctrla`](https://gitlab.inria.fr/control-for-computing/tutorial/-/tree/210423-ctrla))

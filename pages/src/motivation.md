# Motivation

## Motivation of Control for Computing

Computing systems are getting more and more complex.

The software stacks are growing, and are executed on top of complex hardware.
The behavior of applications is thus becoming extremely difficult to predict, or model, correctly.

We need regulation to provide *Quality-of-Service* guarantees.

One approach is to have a theoretical model of the system and design complex algorithm based on this model.
Such an approach is limited by the quality of the model.
Modelling all the possible cases, and behaviors is tedious and error-prone.


A different approach to regulate the behavior of a computing system is to *periodically* take measurements of the system metrics, and adapt the input based on these measurements.
This approach is called *closed-loop*, and this is the interest of the *Autonomic Computing* community.

There are several ways to adapt the input of the system.
One can use ad-hoc solutions based on arbitrary rules or IA for example.

These solutions do not provide any proven guarantees on the closed-loop system.
On the other hand, the *Control Theory* field has been applying math-proven methods to closed-loop physical systems for decades.
But the application of Control Theory to computing systems is only quite recent. 


One example is the one described by Cerf et al. in [Sustaining performance while reducing energy consumption: a control theory approach](https://hal.inria.fr/hal-03259316).
The motivation is the following:
HPC applications are a succession of iterations that are composed of phases.
These phases can be CPU-intensive, IO-intensive, memory-intensive, etc.
During memory-intensive phases, for example, it is useless for the CPU to be at maximum frequency as it will be idle.
Thus, one could decrease the CPU frequency of the compute node with RAPL technology to reduce its energy consumption. 
This decrease might introduce some overhead in the execution time of the application.
The aforementioned paper performs an online regulation of compute node CPU frequency based on an accepted degradation given by the user.

The intuition is the following:
If the application is running slower than desired (too much degradation), the CPU frequency is increased to meet user requirements.
If the application is running faster than desired (too small degradation), the CPU frequency is decreased to reduce energy consumption.

This regulation was done using Control Theory tools and was showed to lead to significant energy saving with 10% degradation levels.


## Motivation of this tutorial

We believe that numerous computing systems or applications could benefit from feedback loops architecture,
and that tools from Control Theory provide interesting guarantees compared to other strategies (ad-hoc, AI, etc.).  

This tutorial aims at introducing the attendees with Control Theory, and present the methodology through a practical example.  

At the end of the tutorial, attendees will have:

- Learned about Control Theory motivations and tools

- Identified a model of a system

- Designed a Proportional Controller, and understood its limitations 

- Designed a more complex controller: a Proportional-Integral Controller

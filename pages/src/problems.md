# Frequent Problems


## The online Jupyter does not execute the notebooks

We are using a WebAssembly implementation of Jupyter to avoid having to manage a server.
However, the WebAssembly technology is still young, and the version your browser might not be recent enough to run it. 

Either you update your browser, or the simplest solution would be to download the notebooks and execute them on your own Jupyter instance.

Download the notebooks [here](https://gitlab.inria.fr/control-for-computing/jupyter/-/jobs/artifacts/master/raw/notebooks_tuto_ctrl.zip?job=archive_notebooks)

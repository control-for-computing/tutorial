# Tutorial Control for Computing

Link to this page: [https://tinyurl.com/CtrlComputing](https://tinyurl.com/Control4Computing)

Link to the slides: [here](https://gitlab.inria.fr/control-for-computing/tutorial/-/jobs/artifacts/master/raw/slides.pdf?job=slides)

## Introduction

This tutorial aims at introducing the tools and notions of the [Control-Theory field](https://en.wikipedia.org/wiki/Control_theory) to computer scientists, and relies on [Jupyter notebooks](https://jupyter.org/), and is composed of two parts:

- In the first part, attendees get familiar with the concepts, tools, and methodology of Control-Theory.
  
- In the second part, attendees are given a pseudo-realistic system and have to design a controller to regulate its behavior.


## Requirements

- a recent web browser (Firefox 90+, Chromium 89+)

- basic `python` knowledge

- basic math knowledge is a plus



<h1 align="center"><b>Start the tutorial <a href="https://control-for-computing.gitlabpages.inria.fr/jupyter/">here</a></b></h1>

# Summary

[Introduction](intro.md)

[Motivation](motivation.md)

[Control Theory](control_theory.md)

[Autonomic Computing](autonomic_computing.md)

[Frequent Problems](problems.md)

[Publications](publications.md)

[Sessions](sessions.md)

[Contacts](contacts.md)

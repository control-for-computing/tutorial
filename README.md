# Tutorial Control for Computing


## Checklist for your tutorial presentation

- Create a notes.inria.fr pad as the entry point.

  - Put the useful links

    - Link to the tutorial: https://control-for-computing.gitlabpages.inria.fr/tutorial/

    - Link to the slides: https://gitlab.inria.fr/control-for-computing/tutorial/-/jobs/artifacts/master/raw/slides.pdf?job=slides

  - Put a section "Comments/Remarks" to encourage feedback 
  
- Use a URL shortener for this pad

  - can be with https://tinyurl.com/app

  - I use the format "Control4Computing@EVENT"

- Put this short URL in the last slide

- Update the slides (`slides/main.md`)

  - Put yourself as the presenter in the slides (`is_presenter`)

  - Update the date

  - Update the session

- Create a `git` tag for your event

  - I started to use this (bad) format `ddmmyy-event` (e.g., `130623-lig`)

  - Add the event to the list of events in the website: `pages/src/sessions.md`

  - generate the `git` tag: `git tag ddmmyy-event`

- Push: `git push --tags`
